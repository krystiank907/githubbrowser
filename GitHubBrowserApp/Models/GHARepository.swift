//
//  GHARepository.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift

class GHARepository: Codable, Equatable {
    var repositoryId: Int = 0
    var name: String = ""
    var fullName: String = ""
    var isprivate: Bool = false
    var htmlUrl: String = ""
    var description: String = ""
    var url: String = ""
    var createdAt: Date?
    var updatedAt: Date?
    var language: String = "None"
    var score: Double = 0
    var owner: GHAOwner?
    var homepage: String?
    var stargazersCount: Int = 0
    var forksCount: Int = 0
    var languageType: GHSearchRepositoriesType? {
        return GHSearchRepositoriesType.allCases.filter({ $0.paramName == language }).first
    }
    private enum CodingKeys: String, CodingKey {
        case name
        case repositoryId = "id"
        case fullName = "full_name"
        case htmlUrl = "html_url"
        case isprivate = "private"
        case description
        case url
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case language
        case score
        case owner
        case homepage
        case stargazersCount = "stargazers_count"
        case forksCount = "forks_count"
    }
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.init()
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        fullName = try container.decodeIfPresent(String.self, forKey: .fullName) ?? ""
        repositoryId = try container.decodeIfPresent(Int.self, forKey: .repositoryId) ?? 0
        isprivate = try container.decodeIfPresent(Bool.self, forKey: .isprivate) ?? false
        description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        url = try container.decodeIfPresent(String.self, forKey: .url) ?? ""
        htmlUrl = try container.decodeIfPresent(String.self, forKey: .htmlUrl) ?? ""
        let dateFormatter = DateFormatter.defaultFormat
        createdAt = dateFormatter.date(from: try container.decodeIfPresent(String.self, forKey: .createdAt) ?? "")
        updatedAt = dateFormatter.date(from: try container.decodeIfPresent(String.self, forKey: .updatedAt) ?? "")
        score = try container.decodeIfPresent(Double.self, forKey: .score) ?? 0
        language = try container.decodeIfPresent(String.self, forKey: .language) ?? "None"
        owner = try container.decodeIfPresent(GHAOwner.self, forKey: .owner) ?? nil
        homepage = try container.decodeIfPresent(String.self, forKey: .homepage) ?? nil
        stargazersCount = try container.decodeIfPresent(Int.self, forKey: .stargazersCount) ?? 0
        forksCount = try container.decodeIfPresent(Int.self, forKey: .forksCount) ?? 0
    }
    static func == (lhs: GHARepository, rhs: GHARepository) -> Bool {
        return lhs.repositoryId == rhs.repositoryId
    }
}

extension GHARepository {
    func saveDataToRealm() {
        do {
            let realm = try Realm()
            realm.autorefresh = true
            let realmObject = RLMFavouriteRepo()
            realmObject.name = name
            realmObject.fullName = fullName
            realmObject.favouriteRepoId = repositoryId
            realmObject.isprivate = isprivate
            realmObject.descriptionRepo = description
            realmObject.url = url
            realmObject.htmlUrl = htmlUrl
            realmObject.createdAt = createdAt
            realmObject.updatedAt = updatedAt
            realmObject.score = score
            realmObject.language = language
            realmObject.homepage = homepage
            let owner = self.owner?.mapToRealmObject()
            realmObject.owner = owner
            realmObject.stargazersCount = stargazersCount
            realmObject.forksCount = forksCount
            try realm.write {
                realm.add(realmObject, update: .all)
                if let owner = owner {
                    realm.add(owner)
                }
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
