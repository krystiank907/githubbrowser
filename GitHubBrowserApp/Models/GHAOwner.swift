//
//  GHAOwner.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 22/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

class GHAOwner: Codable {
    var ownerId: Int = 0
    var login: String = ""
    var nodeId: String = ""
    var avatarUrl: String = ""
    var url: String = ""
    var receivedEventsUrl: String = ""
    var type: String = ""

    private enum CodingKeys: String, CodingKey {
        case ownerId = "id"
        case login
        case nodeId = "node_id"
        case avatarUrl = "avatar_url"
        case url
        case receivedEventsUrl = "received_events_url"
        case type
    }
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.init()
        ownerId = try container.decodeIfPresent(Int.self, forKey: .ownerId) ?? 0
        login = try container.decodeIfPresent(String.self, forKey: .login) ?? ""
        nodeId = try container.decodeIfPresent(String.self, forKey: .nodeId) ?? ""
        avatarUrl = try container.decodeIfPresent(String.self, forKey: .avatarUrl) ?? ""
        url = try container.decodeIfPresent(String.self, forKey: .url) ?? ""
        receivedEventsUrl = try container.decodeIfPresent(String.self, forKey: .receivedEventsUrl) ?? ""
        type = try container.decodeIfPresent(String.self, forKey: .type) ?? ""
    }
}

extension GHAOwner {
    func mapToRealmObject() -> RLMOwner {
        let realmObject = RLMOwner()
        realmObject.ownerId = ownerId
        realmObject.nodeId = nodeId
        realmObject.login = login
        realmObject.avatarUrl = avatarUrl
        realmObject.receivedEventsUrl = receivedEventsUrl
        realmObject.url = url
        realmObject.type = type
        return realmObject
    }
}
