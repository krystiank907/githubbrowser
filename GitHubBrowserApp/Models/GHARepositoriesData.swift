//
//  GHARepositoriesData.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

class GHARepositoriesData: Codable {
    var totalCount: Int = 0
    var incompleteResults = false
    var paramSend: GHSearchParams?
    var items: [GHARepository] = []
    private enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case incompleteResults = "incomplete_results"
        case items
    }
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.init()
        totalCount = try container.decodeIfPresent(Int.self, forKey: .totalCount) ?? 0
        incompleteResults = try container.decodeIfPresent(Bool.self, forKey: .incompleteResults) ?? false
        items = try container.decodeIfPresent([GHARepository].self, forKey: .items) ?? []
    }
}
