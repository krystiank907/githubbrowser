//
//  RLMOwner.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift

class RLMOwner: Object {
    @objc dynamic var ownerId: Int = 0
    @objc dynamic var login: String = ""
    @objc dynamic var nodeId: String = ""
    @objc dynamic var avatarUrl: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var receivedEventsUrl: String = ""
    @objc dynamic var type: String = ""
    override class func primaryKey() -> String? {
        return "ownerId"
    }
    func mapToLocalObject() -> GHAOwner {
        let localObject = GHAOwner()
        localObject.ownerId = ownerId
        localObject.nodeId = nodeId
        localObject.login = login
        localObject.avatarUrl = avatarUrl
        localObject.receivedEventsUrl = receivedEventsUrl
        localObject.url = url
        localObject.type = type
        return localObject
    }
}
