//
//  RLMFavouriteRepo.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift

class RLMFavouriteRepo: Object {
    @objc dynamic var favouriteRepoId: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var fullName: String = ""
    @objc dynamic var isprivate: Bool = false
    @objc dynamic var htmlUrl: String = ""
    @objc dynamic var descriptionRepo: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var createdAt: Date?
    @objc dynamic var updatedAt: Date?
    @objc dynamic var language: String = "None"
    @objc dynamic var score: Double = 0
    @objc dynamic var homepage: String?
    @objc dynamic var stargazersCount: Int = 0
    @objc dynamic var forksCount: Int = 0
    @objc dynamic var owner: RLMOwner?
    override class func primaryKey() -> String? {
        return "favouriteRepoId"
    }
    var languageType: GHSearchRepositoriesType? {
        return GHSearchRepositoriesType.allCases.filter({$0.paramName == language}).first
    }
    func mapToLocalObject() -> GHARepository {
        let localObject = GHARepository()
        localObject.name = name
        localObject.fullName = fullName
        localObject.repositoryId = favouriteRepoId
        localObject.isprivate = isprivate
        localObject.description = descriptionRepo
        localObject.url = url
        localObject.htmlUrl = htmlUrl
        localObject.createdAt = createdAt
        localObject.updatedAt = updatedAt
        localObject.score = score
        localObject.language = language
        localObject.homepage = homepage
        if let owner = self.owner {
            localObject.owner = owner.mapToLocalObject()
        }
        localObject.stargazersCount = stargazersCount
        localObject.forksCount = forksCount
        return localObject
    }

}
