//
//  GHAWebDetialViewController.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 14/02/2020.
//  Copyright © 2020 Krystian Kulawiak. All rights reserved.
//

import UIKit
import JGProgressHUD
import WebKit

class GHAWebDetialViewController: GHABaseViewController, ViewControllerAutoCreateViews {
    weak var webView: WKWebView!
    //sourcery:begin: notCreate
    private var repoURL: URL
    private let progressHUD = JGProgressHUD(style: .light)
    //sourcery:end
    init(repoURL: URL) {
        self.repoURL = repoURL
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        createViews()
        setupViews()
        createConstraints()
    }
    func setupViews() {
        progressHUD.show(in: webView, animated: true)
        let urlRequest = URLRequest(url: repoURL)
        webView.navigationDelegate = self
        webView.load(urlRequest)
    }
    func createConstraints() {
        NSLayoutConstraint.activate(webView.constraintsEqualToSupperView())
    }
}
extension GHAWebDetialViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressHUD.dismiss(animated: true)
    }
}
