//
//  GHAReposiotryDetailViewController.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import DevStart

class GHAReposiotryDetailViewController: GHABaseViewController, DSTableViewControllerWithViewModel {
    var viewModel: VMRepositoryDetail!
    lazy private var favouriteButton = UIBarButtonItem(
                                            image: UIImage(named:"noFavourite")?.withRenderingMode(.alwaysOriginal),
                                               style: .done,
                                               target: self,
                                               action: #selector(didSelectFavouriteButton))
    init(viewModel: VMRepositoryDetail) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        tableViewDataSource = DSTableViewDataSource(viewModel: viewModel, viewController: self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
    private func setupNavBar() {
        setTitleNavBar(viewModel.repositoryItem.owner?.login ?? "")
        navigationItem.rightBarButtonItem = favouriteButton
        setupFavouriteButton()
    }
    @objc private  func didSelectFavouriteButton() {
        viewModel.favouriteButtonAction()
        setupFavouriteButton()
    }
    private func setupFavouriteButton() {
        let image = viewModel.itemIsFavourite ? UIImage(named: "favourite") : UIImage(named: "noFavourite")
        favouriteButton.image = image?.withRenderingMode(.alwaysOriginal)
    }
    func setupCell(_ tableView: UITableView, cell: UITableViewCell, at indexPath: IndexPath) {
        if let headerCell = cell as? GHARepoDetailHeaderCell {
            headerCell.setupCell(object: viewModel.repositoryItem)
        } else if let stateCell = cell as? GHRepoDetailStatsCell {
            stateCell.setupCell(object: viewModel.repositoryItem)
        } else if let buttonCell = cell as? GHAButtonCell {
            buttonCell.setupCell(buttonTitle: "GHAReposiotryDetailViewController_openWeb".localized)
        }
    }
}

extension GHAReposiotryDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = GHARepositoryDetailSections(rawValue: indexPath.section)
        guard let unnwrapedSection = section else {
            return
        }
        switch unnwrapedSection {
        case .openWeb:
            guard let url = URL(string: viewModel.repositoryItem.htmlUrl) else {return }
            let viewC = GHAWebDetialViewController(repoURL: url)
            navigationController?.pushViewController(viewC, animated: true)
        default:
            break
        }
    }
}
