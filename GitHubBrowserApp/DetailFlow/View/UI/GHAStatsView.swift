//
//  GHAStatsView.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import DevStart

class GHAStatsView: UIView, AutoCreateViews {
    weak var iconStatImageView: UIImageView!
    weak var countLabel: UILabel!
    init(image: UIImage?, text: String) {
        super.init(frame: .zero)
        createViews()
        setupViews()
        createConstraints()
        countLabel.text = text
        iconStatImageView.image = image
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupViews() {
        countLabel.font = UIFont.appFont(ofSize: 16, weight: .semibold)
        countLabel.textColor = .black
    }
    func createConstraints() {
        let viewsDict = self.viewsDictionary
        var constraints: [NSLayoutConstraint] = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[iconStatImageView(25)]-[countLabel]-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|->=0-[countLabel]->=0@750-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-14-[iconStatImageView(25)]-14-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict)
            ].flatMap {$0 }
        constraints.append(contentsOf: [
            countLabel.centerYAnchor.constraint(equalTo: iconStatImageView.centerYAnchor)
        ])
        NSLayoutConstraint.activate(constraints)
    }
}
