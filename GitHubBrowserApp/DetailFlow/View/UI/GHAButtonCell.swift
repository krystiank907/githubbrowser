//
//  GHAButtonCell.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 14/02/2020.
//  Copyright © 2020 Krystian Kulawiak. All rights reserved.
//
import UIKit
import RxSwift
import RxCocoa

class GHAButtonCell: UITableViewCell, CellAutoCreateViews {
    weak var button: UIButton!
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createViews()
        setupViews()
        createConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupCell(buttonTitle: String) {
        button.setTitle(buttonTitle, for: .normal)
    }
}
extension GHAButtonCell {
    func setupViews() {
        button.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        button.clipsToBounds = true
        button.layer.cornerRadius = 11
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.appFont(ofSize: 16, weight: .semibold)
        button.isUserInteractionEnabled = false
    }
    func createConstraints() {
        var constraints = button.constraintsEqualToSupperView(UIEdgeInsets(top: 15, left: 15, bottom: 0, right: 15))
        constraints.append(button.heightAnchor.constraint(equalToConstant: 40))
        NSLayoutConstraint.activate(constraints)
    }
}
