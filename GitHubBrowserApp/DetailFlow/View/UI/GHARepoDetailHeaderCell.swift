//
//  GHARepoDetailHeaderCell.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RxSwift
import AlamofireImage

class GHARepoDetailHeaderCell: UITableViewCell, CellAutoCreateViews {
    weak var containerView: UIView!
    //sourcery:begin: superView = containerView
    weak var titleLabel: UILabel!
    weak var descriptionLabel: UILabel!
    weak var userImageView: UIImageView!
    weak var openWebSiteButton: UIButton!
    //sourcery:end
    //sourcery:begin: notCreate
    var disposeBag = DisposeBag()
    //sourcery:end
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createViews()
        setupViews()
        createConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupCell(object: GHARepository) {
        titleLabel.text = object.fullName
        descriptionLabel.text = object.description
        if let stringURL = object.owner?.avatarUrl, let url = URL(string: stringURL) {
            userImageView.af_setImage(withURL: url)
        }
        containerView.backgroundColor = object.languageType?.color.withAlphaComponent(0.8) ?? UIColor(red: 0.01,
                                                                                                      green: 0.05,
                                                                                                      blue: 0.16,
                                                                                                      alpha: 0.8)
        openWebSiteButton.setAttributedTitle(NSAttributedString(string: object.homepage ?? "", attributes: [
            .font: UIFont.appFont(ofSize: 14, weight: .light),
            .foregroundColor: UIColor.white
        ]), for: .normal)
        openWebSiteButton.rx.tap.subscribe(onNext: { _ in
            guard let url = URL(string: object.homepage ?? "") else { return }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }).disposed(by: disposeBag)
    }
}
extension GHARepoDetailHeaderCell {
    func setupViews() {
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 11
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 1
        titleLabel.font = UIFont.appFont(ofSize: 17, weight: .semibold)
        titleLabel.lineBreakMode = .byTruncatingTail
        descriptionLabel.textColor = .lightGray
        descriptionLabel.numberOfLines = 8
        descriptionLabel.lineBreakMode = .byWordWrapping
        containerView.addShadow(4)
        userImageView.clipsToBounds = true
        userImageView.layer.cornerRadius = 35
    }
    func createConstraints() {
        let viewsDict = self.viewsDictionary
        let constraints: [NSLayoutConstraint] = [
            NSLayoutConstraint.constraints(withVisualFormat:
                "V:|-16-[titleLabel]-[descriptionLabel]-[openWebSiteButton(30)]-|",
                                           options: [.alignAllLeading, .alignAllTrailing],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-16-[userImageView(70)]->=9-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-19-[userImageView(70)]-19-[titleLabel]-19-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            containerView.constraintsEqualToSupperView(UIEdgeInsets(top: 15, left: 15, bottom: 0, right: 15))
            ].flatMap {$0 }
        NSLayoutConstraint.activate(constraints)
    }
}
