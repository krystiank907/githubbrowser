//
//  GHRepoDetailStatsCell.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GHRepoDetailStatsCell: UITableViewCell, CellAutoCreateViews {
    weak var containerView: UIView!
    //sourcery:begin: superView = containerView
    weak var itemsStackView: UIStackView!
    //sourcery:end
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createViews()
        setupViews()
        createConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupCell(object: GHARepository) {
        GHAStatsType.allCases.forEach { item in
            var count = 0
            switch item {
            case .score:
                count = Int(object.score)
            case .forks:
                count = object.forksCount
            case .stars:
                count = object.stargazersCount
            }
            let stateView = GHAStatsView(image: item.iconImage, text: "\(item.name): \(count)")
            itemsStackView.addArrangedSubview(stateView)
        }
    }
}
extension GHRepoDetailStatsCell {
    func setupViews() {
        containerView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 11
        itemsStackView.axis = .vertical
        itemsStackView.distribution = .equalSpacing
    }
    func createConstraints() {
        let viewsDict = self.viewsDictionary
        let constraints: [NSLayoutConstraint] = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[itemsStackView]-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[itemsStackView]-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            containerView.constraintsEqualToSupperView(UIEdgeInsets(top: 15, left: 15, bottom: 0, right: 15))
            ].flatMap {$0 }
        NSLayoutConstraint.activate(constraints)
    }
}
