//
//  VMRepositoryDetail.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import DevStart
import RealmSwift

class VMRepositoryDetail: DSViewModelTableDataSource {
    var repositoryItem: GHARepository
    init(repositoryItem: GHARepository) {
        self.repositoryItem = repositoryItem
    }
    var tableItemIdentifiers: [String: AnyClass] {
        return [
            GHARepoDetailHeaderCell.className: GHARepoDetailHeaderCell.self,
            GHRepoDetailStatsCell.className: GHRepoDetailStatsCell.self,
            GHAButtonCell.className: GHAButtonCell.self
        ]
    }
    var numberOfTableSections: Int {
        return GHARepositoryDetailSections.allCases.count
    }
    func identifierForTableItem(atIndexPath indexPath: IndexPath) -> String {
        let section = GHARepositoryDetailSections(rawValue: indexPath.section)
        guard let unnwrapedSection = section else {
            return UITableViewCell.className
        }
        switch unnwrapedSection {
        case .header:
            return GHARepoDetailHeaderCell.className
        case .state:
            return GHRepoDetailStatsCell.className
        case .openWeb:
            return GHAButtonCell.className
        }
    }
    func numberOfItemsInTableSection(_ section: Int) -> Int {
        return 1
    }
    private var realmFavouriteRepoObject: RLMFavouriteRepo? {
        do {
            let realm = try Realm()
            let object = realm.object(ofType: RLMFavouriteRepo.self, forPrimaryKey: repositoryItem.repositoryId)
            return object
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    var itemIsFavourite: Bool {
        return realmFavouriteRepoObject != nil
    }
    func favouriteButtonAction() {
        do {
            let realm = try Realm()
            realm.autorefresh = true
            if let realmFavouriteRepoObject = realmFavouriteRepoObject {
                try realm.write {
                    if let owner = realmFavouriteRepoObject.owner {
                        realm.delete(owner)
                    }
                    realm.delete(realmFavouriteRepoObject)
                }
            } else {
                repositoryItem.saveDataToRealm()
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
