//
//  GHARepositoryDetailSections.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

enum GHARepositoryDetailSections: Int, CaseIterable {
    case header = 0, state, openWeb
}
