//
//  GHAStatsType.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

enum GHAStatsType: Int, CaseIterable {
    case stars = 0
    case forks
    case score
    var name: String {
        return "GHAStatsType_name_\(self)".localized
    }
    var iconImage: UIImage? {
        return UIImage(named: "GHAStatsType_\(self)")
    }
}
