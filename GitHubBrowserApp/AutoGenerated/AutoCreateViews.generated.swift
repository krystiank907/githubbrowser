// Generated using Sourcery 0.16.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

import UIKit
import WebKit
// MARK: - Create Views for GHAButtonCell
extension GHAButtonCell {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["button"] = button
        return dictionary
    }
    @objc dynamic func createViews() {
        let button = UIButton(type: .system)   
        contentView.addSubview(button)
        self.button = button
        button.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Create Views for GHARepoDetailHeaderCell
extension GHARepoDetailHeaderCell {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["containerView"] = containerView
        dictionary["titleLabel"] = titleLabel
        dictionary["descriptionLabel"] = descriptionLabel
        dictionary["userImageView"] = userImageView
        dictionary["openWebSiteButton"] = openWebSiteButton
        return dictionary
    }
    @objc dynamic func createViews() {
        let containerView = UIView()
        contentView.addSubview(containerView)
        self.containerView = containerView
        containerView.translatesAutoresizingMaskIntoConstraints = false
        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = .appFont()
        containerView.addSubview(titleLabel)
        self.titleLabel = titleLabel
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        let descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.font = .appFont()
        containerView.addSubview(descriptionLabel)
        self.descriptionLabel = descriptionLabel
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        let userImageView = UIImageView()
        containerView.addSubview(userImageView)
        self.userImageView = userImageView
        userImageView.translatesAutoresizingMaskIntoConstraints = false
        let openWebSiteButton = UIButton(type: .system)   
        containerView.addSubview(openWebSiteButton)
        self.openWebSiteButton = openWebSiteButton
        openWebSiteButton.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Create Views for GHARepoSearchCell
extension GHARepoSearchCell {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["containerView"] = containerView
        dictionary["titleLabel"] = titleLabel
        dictionary["descriptionLabel"] = descriptionLabel
        dictionary["rightSeparatorView"] = rightSeparatorView
        dictionary["updateDateLabel"] = updateDateLabel
        dictionary["languageVerticalLabel"] = languageVerticalLabel
        return dictionary
    }
    @objc dynamic func createViews() {
        let containerView = UIView()
        contentView.addSubview(containerView)
        self.containerView = containerView
        containerView.translatesAutoresizingMaskIntoConstraints = false
        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = .appFont()
        containerView.addSubview(titleLabel)
        self.titleLabel = titleLabel
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        let descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.font = .appFont()
        containerView.addSubview(descriptionLabel)
        self.descriptionLabel = descriptionLabel
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        let rightSeparatorView = UIView()
        containerView.addSubview(rightSeparatorView)
        self.rightSeparatorView = rightSeparatorView
        rightSeparatorView.translatesAutoresizingMaskIntoConstraints = false
        let updateDateLabel = UILabel()
        updateDateLabel.numberOfLines = 0
        updateDateLabel.lineBreakMode = .byWordWrapping
        updateDateLabel.font = .appFont()
        containerView.addSubview(updateDateLabel)
        self.updateDateLabel = updateDateLabel
        updateDateLabel.translatesAutoresizingMaskIntoConstraints = false
        let languageVerticalLabel = GHAVerticalLabel()
        rightSeparatorView.addSubview(languageVerticalLabel)
        self.languageVerticalLabel = languageVerticalLabel
        languageVerticalLabel.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Create Views for GHASearchOptionCell
extension GHASearchOptionCell {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["titleLabel"] = titleLabel
        dictionary["switchControl"] = switchControl
        dictionary["separatorView"] = separatorView
        return dictionary
    }
    @objc dynamic func createViews() {
        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = .appFont()
        contentView.addSubview(titleLabel)
        self.titleLabel = titleLabel
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        let switchControl = UISwitch()
        contentView.addSubview(switchControl)
        self.switchControl = switchControl
        switchControl.translatesAutoresizingMaskIntoConstraints = false
        let separatorView = UIView()
        contentView.addSubview(separatorView)
        self.separatorView = separatorView
        separatorView.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Create Views for GHRepoDetailStatsCell
extension GHRepoDetailStatsCell {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["containerView"] = containerView
        dictionary["itemsStackView"] = itemsStackView
        return dictionary
    }
    @objc dynamic func createViews() {
        let containerView = UIView()
        contentView.addSubview(containerView)
        self.containerView = containerView
        containerView.translatesAutoresizingMaskIntoConstraints = false
        let itemsStackView = UIStackView()
        containerView.addSubview(itemsStackView)
        self.itemsStackView = itemsStackView
        itemsStackView.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Create Views for GHARepoFavouriteViewController
extension GHARepoFavouriteViewController {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["emptyView"] = emptyView
        return dictionary
    }
    @objc dynamic func createViews() {
        let emptyView = GHAEmptyView()
        view.addSubview(emptyView)
        self.emptyView = emptyView
        emptyView.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Create Views for GHASearchTableViewController
extension GHASearchTableViewController {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["headerContainerView"] = headerContainerView
        dictionary["emptyView"] = emptyView
        dictionary["searchBar"] = searchBar
        dictionary["filterButton"] = filterButton
        dictionary["totalResultsLabel"] = totalResultsLabel
        return dictionary
    }
    @objc dynamic func createViews() {
        let headerContainerView = UIView()
        view.addSubview(headerContainerView)
        self.headerContainerView = headerContainerView
        headerContainerView.translatesAutoresizingMaskIntoConstraints = false
        let emptyView = GHAEmptyView()
        view.addSubview(emptyView)
        self.emptyView = emptyView
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        let searchBar = GHASearchView()
        headerContainerView.addSubview(searchBar)
        self.searchBar = searchBar
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        let filterButton = UIButton(type: .system)
        headerContainerView.addSubview(filterButton)
        self.filterButton = filterButton
        filterButton.translatesAutoresizingMaskIntoConstraints = false
        let totalResultsLabel = UILabel()
        totalResultsLabel.numberOfLines = 0
        totalResultsLabel.lineBreakMode = .byWordWrapping
        totalResultsLabel.font = .appFont()
        headerContainerView.addSubview(totalResultsLabel)
        self.totalResultsLabel = totalResultsLabel
        totalResultsLabel.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Create Views for GHAWebDetialViewController
extension GHAWebDetialViewController {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["webView"] = webView
        return dictionary
    }
    @objc dynamic func createViews() {
        let webView = WKWebView()
        view.addSubview(webView)
        self.webView = webView
        webView.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Create Views for GHAEmptyView
extension GHAEmptyView {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["iconGitImageView"] = iconGitImageView
        dictionary["descriptionLabel"] = descriptionLabel
        return dictionary
    }
    @objc dynamic func createViews() {
        let iconGitImageView = UIImageView()
        addSubview(iconGitImageView)
        self.iconGitImageView = iconGitImageView
        iconGitImageView.translatesAutoresizingMaskIntoConstraints = false
        let descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.font = .appFont()
        addSubview(descriptionLabel)
        self.descriptionLabel = descriptionLabel
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Create Views for GHASearchView
extension GHASearchView {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["bacgroundView"] = bacgroundView
        dictionary["textField"] = textField
        dictionary["iconImageView"] = iconImageView
        return dictionary
    }
    @objc dynamic func createViews() {
        let bacgroundView = UIView()
        addSubview(bacgroundView)
        self.bacgroundView = bacgroundView
        bacgroundView.translatesAutoresizingMaskIntoConstraints = false
        let textField = GHATextField()
        bacgroundView.addSubview(textField)
        self.textField = textField
        textField.translatesAutoresizingMaskIntoConstraints = false
        let iconImageView = UIImageView()
        bacgroundView.addSubview(iconImageView)
        self.iconImageView = iconImageView
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Create Views for GHAStatsView
extension GHAStatsView {
    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["iconStatImageView"] = iconStatImageView
        dictionary["countLabel"] = countLabel
        return dictionary
    }
    @objc dynamic func createViews() {
        let iconStatImageView = UIImageView()
        addSubview(iconStatImageView)
        self.iconStatImageView = iconStatImageView
        iconStatImageView.translatesAutoresizingMaskIntoConstraints = false
        let countLabel = UILabel()
        countLabel.numberOfLines = 0
        countLabel.lineBreakMode = .byWordWrapping
        countLabel.font = .appFont()
        addSubview(countLabel)
        self.countLabel = countLabel
        countLabel.translatesAutoresizingMaskIntoConstraints = false
    }
}
