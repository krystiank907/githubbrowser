//
//  AutoGenerateProtocols.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

protocol CellAutoCreateViews {
    func setupViews()
    func createConstraints()
}

protocol ViewControllerAutoCreateViews {
    func setupViews()
    func createConstraints()
}

protocol AutoCreateViews {
    func setupViews()
    func createConstraints()
}

public protocol AutoMapping {
}
