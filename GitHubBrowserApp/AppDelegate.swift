//
//  AppDelegate.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RealmSwift
import AlamofireImage
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        #if DEBUG
        print(Realm.Configuration.defaultConfiguration.fileURL ?? "")
        #endif
        createRootViewController()
        appearanceConfiguration()
        setupNetworkLogging()
        return true
    }
    func setupNetworkLogging() {
//        #if DEBUG
//            NetworkActivityLogger.shared.level = .debug
//            NetworkActivityLogger.shared.startLogging()
//        #endif
    }
    func createRootViewController() {
        if window == nil {
            window = UIWindow()
        }
        window?.makeKeyAndVisible()
        let rootVC = GHARootViewController()
        window?.rootViewController = rootVC
    }
    func appearanceConfiguration() {
        UITableViewCell.appearance().selectionStyle = .none
        let navigationBar = UINavigationBar.appearance()
        navigationBar.barStyle = .default
        navigationBar.barTintColor = .ghaBlack
        navigationBar.backgroundColor = .ghaBlack
        navigationBar.tintColor = .white
        navigationBar.titleTextAttributes = [
            .font: UIFont.appFont(ofSize: 16, weight: .bold),
            .foregroundColor: UIColor.white
        ]
        navigationBar.largeTitleTextAttributes = [
            .font: UIFont.appFont(ofSize: 30, weight: .bold),
            .foregroundColor: UIColor.white
        ]
        let tabbar = UITabBar.appearance()
        tabbar.isTranslucent = false
        tabbar.barTintColor = .ghaBlack
        let tabbarItem = UITabBarItem.appearance()
        tabbarItem.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont.appFont(ofSize: 12, weight: .bold),
            NSAttributedString.Key.foregroundColor: UIColor.ghaOrange
            ], for: .selected)
        tabbarItem.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont.appFont(ofSize: 12),
            NSAttributedString.Key.foregroundColor: UIColor.white
            ], for: .normal)
    }
}
