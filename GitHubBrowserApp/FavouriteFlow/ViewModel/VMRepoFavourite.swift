//
//  VMRepoFavourite.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import DevStart
import RealmSwift

class VMRepoFavourite: DSViewModelRealmTableDataSource {
    var tableRealmDataArray: AnyRealmCollection<RLMFavouriteRepo>!
    init() {
        do {
            let realm = try Realm()
            let dataArray = realm.objects(RLMFavouriteRepo.self)
            tableRealmDataArray = AnyRealmCollection(dataArray)
        } catch {
            print(error.localizedDescription)
        }
    }
    var tableItemIdentifiers: [String: AnyClass] {
        return [GHARepoSearchCell.className: GHARepoSearchCell.self]
    }
}
