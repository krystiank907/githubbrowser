//
//  GHARepoFavouriteViewController.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import DevStart
import RxCocoa
import JGProgressHUD
import RxSwift

class GHARepoFavouriteViewController: GHABaseViewController, DSTableViewControllerWithViewModel {
    weak var emptyView: GHAEmptyView!
    //sourcery:begin: notCreate
    override var prefersNavigationBarHidden: Bool { return false }
    var viewModel: VMRepoFavourite!
    //sourcery:end
    init(viewModel: VMRepoFavourite = .init()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        tableViewDataSource = DSTableViewDataSource(viewModel: viewModel, viewController: self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        createConstraints()
        observeDataViewModel()
        setTitleNavBar("Favourites")
    }
    private func observeDataViewModel() {
        Observable.collection(from: viewModel.tableRealmDataArray)
            .subscribe(onNext: { [weak self] _ in
            self?.reloadData()
        }).disposed(by: disposeBag)
    }
    func setupCell(_ tableView: UITableView, cell: UITableViewCell, at indexPath: IndexPath) {
        guard let repoCell = cell as? GHARepoSearchCell,
            let object = viewModel.tableObjectForIndexPath(indexPath) else {return }
        let localObject = object.mapToLocalObject()
        repoCell.setupCell(object: localObject)
    }
    private func reloadData() {
        setupEmptyView()
        UIView.transition(with: tableView,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: { self.tableView.reloadData() },
                          completion: nil)
    }
}
extension GHARepoFavouriteViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let object = viewModel.tableObjectForIndexPath(indexPath) else {return }
        let viewModel = VMRepositoryDetail(repositoryItem: object.mapToLocalObject())
        let viewC = GHAReposiotryDetailViewController(viewModel: viewModel)
        navigationController?.pushViewController(viewC, animated: true)
    }
}
extension GHARepoFavouriteViewController: ViewControllerAutoCreateViews {
    func setupViews() {
        setupTableView()
        setupEmptyView()
    }
    private func setupEmptyView() {
        var shouldHiddeView = false
        if viewModel.tableItemsCount == 0 {
            shouldHiddeView = false
            emptyView.setupView("GHARepoFavouriteViewController_emptyFavourite".localized)
        } else {
            shouldHiddeView = true
        }
        if emptyView.isHidden != shouldHiddeView {
            UIView.transition(with: emptyView,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: { self.emptyView.isHidden = shouldHiddeView },
                              completion: nil)
        }
    }
    func createConstraints() {
        var viewsDict = self.viewsDictionary
        viewsDict["tableView"] = tableView
        var constraints: [NSLayoutConstraint] = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[tableView]|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[tableView]|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[emptyView]-20-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|->=0-[emptyView]->=0@750-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict)
            ].flatMap {$0 }
        constraints.append(contentsOf: [
            emptyView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
            tableView.topAnchor.constraint(equalTo: safeTopAnchor, constant: 0)
        ])
        NSLayoutConstraint.activate(constraints)
    }
}
