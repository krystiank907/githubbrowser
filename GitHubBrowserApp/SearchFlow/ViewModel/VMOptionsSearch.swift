//
//  VMOptionsSearch.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import DevStart
import RxSwift
import RxCocoa

class VMOptionsSearch: DSViewModelTableDataSource {
    var searchOptions: BehaviorRelay<GHASearchOptions>
    init(searchOptions: BehaviorRelay<GHASearchOptions>) {
        self.searchOptions = searchOptions
    }
    var tableItemIdentifiers: [String: AnyClass] {
        return [GHASearchOptionCell.className: GHASearchOptionCell.self]
    }
    var numberOfTableSections: Int {
        return GHAOptionsSearchSections.allCases.count
    }
    func numberOfItemsInTableSection(_ section: Int) -> Int {
        guard let sectionItem = GHAOptionsSearchSections(rawValue: section) else {
            return 0
        }
        switch sectionItem {
        case .filters:
            return GHSearchRepositoriesType.allCases.count
        case .sort:
            return GHSearchRepositoriesSortType.allCases.count
        }
    }
    func vmForSwitchCell(at indexPath: IndexPath) -> VMSearchOptionCell? {
        guard let objectForSection = itemSection(for: indexPath) else {
            return nil
        }
        if let cellItem = objectForSection as? GHSearchRepositoriesType {
            let isOn = searchOptions.value.filterOptionsSelected == cellItem
            return VMSearchOptionCell(titleText: cellItem.paramName, isOn: isOn)
        } else if let cellItem = objectForSection as? GHSearchRepositoriesSortType {
            let isOn = searchOptions.value.sortOption == cellItem
            return VMSearchOptionCell(titleText: cellItem.name, isOn: isOn)
        } else {
            return nil
        }
    }
    private func itemSection(for indexPath: IndexPath) -> Any? {
        guard let sectionItem = GHAOptionsSearchSections(rawValue: indexPath.section) else {
            return nil
        }
        switch sectionItem {
        case .filters:
            return GHSearchRepositoriesType(rawValue: indexPath.row)
        case .sort:
            return GHSearchRepositoriesSortType(rawValue: indexPath.row)
        }
    }
    func setState(for indexPath: IndexPath) {
        guard let objectForSection = itemSection(for: indexPath) else {
            return
        }
        var optionsValue = searchOptions.value
        if let cellItem = objectForSection as? GHSearchRepositoriesType {
            optionsValue.filterOptionsSelected = cellItem
        } else if let cellItem = objectForSection as? GHSearchRepositoriesSortType {
            optionsValue.sortOption = cellItem
        }
        searchOptions.accept(optionsValue)
    }
    func headerTitleForSection(_ section: Int) -> String? {
        return GHAOptionsSearchSections(rawValue: section)?.title
    }
}

struct GHASearchOptions {
    var filterOptionsSelected: GHSearchRepositoriesType
    var sortOption: GHSearchRepositoriesSortType
}
