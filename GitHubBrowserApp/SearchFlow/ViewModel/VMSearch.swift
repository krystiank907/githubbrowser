//
//  VMSearch.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import DevStart
import RxSwift
import RxCocoa

class VMSearch {
    private var downloadManager = GHADownloadManager.shered
    var repositoryData = BehaviorRelay<GHARepositoriesData?>(value: nil)
    var searchString = BehaviorRelay<String?>(value: nil)
    var shouldShowLoader = BehaviorRelay<Bool>(value: false)
    var downloadDataQueExist: Bool = false
    var searchOptions: BehaviorRelay<GHASearchOptions>
    init() {
        let searchOptions = GHASearchOptions(filterOptionsSelected: .all, sortOption: .bestMatch)
        self.searchOptions = BehaviorRelay<GHASearchOptions>(value: searchOptions)
        observeData()
    }
    private func observeData() {
        searchString.distinctUntilChanged().subscribe(onNext: {[weak self] _ in
            self?.repositoryData.accept(nil)
            self?.getDataForSearch()
        }).disposed(by: disposeBag)
        searchOptions.subscribe(onNext: {[weak self] _ in
            self?.repositoryData.accept(nil)
            self?.getDataForSearch()
        }).disposed(by: disposeBag)
    }
    private lazy var itemsPerPage: Int = {
        return 30
    }()
    //get total available pages for actual searchString
    private var totalPages: Int {
        let totalItems = repositoryData.value?.totalCount ?? 0
        let valuePage: Double = Double(totalItems / itemsPerPage)
        if floor(valuePage) == valuePage {
            return Int(valuePage)
        } else {
            return Int(valuePage + 1)
        }
    }
    //info about end of pages in data
    private var shouldDownloadMoreData: Bool {
        if let value = repositoryData.value {
            return value.items.count != value.totalCount
        } else {
            return true
        }
    }
    //get current page for download data
    private var currenPageForDownload: Int {
        if repositoryData.value?.items.isEmpty ?? true {
            return 1
        } else if let value = repositoryData.value {
            let pagesDownloaded = value.items.count / itemsPerPage
            return Int(pagesDownloaded + 1)
        } else {
            return 1
        }
    }
    //parameters for search repositroy
    private var paramRepo: GHSearchParams? {
        guard let searchString = searchString.value, !searchString.isEmpty else {return nil}
        var param = GHSearchParams(searchString: searchString)
        param.perPage = itemsPerPage
        param.languageType = searchOptions.value.filterOptionsSelected
        param.sortType = searchOptions.value.sortOption
        param.page = currenPageForDownload
        return param
    }
    //Download data for searching text
    func getDataForSearch() {
        if downloadDataQueExist || !shouldDownloadMoreData {
            return
        }
        guard let param = paramRepo else {return }
        downloadDataQueExist = true
        shouldShowLoader.accept(true)
        downloadManager.downloadData(methodInfo: .searchRepository(params: param)) { [weak self] (completion) in
            self?.downloadDataQueExist = false
            self?.shouldShowLoader.accept(false)
            guard let obj = completion.data as? GHARepositoriesData else { return }
            var previousDataItems = self?.repositoryData.value?.items ?? []
            previousDataItems.append(contentsOf: obj.items)
            let data = obj
            data.items = previousDataItems.removeDuplicates()
            obj.paramSend = self?.paramRepo
            self?.repositoryData.accept(data)
        }
    }
}

//Datasource for TableView
extension VMSearch: DSViewModelTableDataSource {
    var tableItemIdentifiers: [String: AnyClass] {
        return [GHARepoSearchCell.className: GHARepoSearchCell.self]
    }
    func numberOfItemsInTableSection(_ section: Int) -> Int {
        return repositoryData.value?.items.count ?? 0
    }
}
