//
//  GHASearchTableViewController.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import DevStart
import RxCocoa
import JGProgressHUD
import RxSwift

class GHASearchTableViewController: GHABaseViewController, DSTableViewControllerWithViewModel {
    weak var headerContainerView: UIView!
    weak var emptyView: GHAEmptyView!
    //sourcery:begin: superView = headerContainerView
    weak var searchBar: GHASearchView!
    weak var filterButton: UIButton!
    weak var totalResultsLabel: UILabel!
    //sourcery:end
    //sourcery:begin: notCreate
    override var prefersNavigationBarHidden: Bool { return true }
    var viewModel: VMSearch!
    private let hud = JGProgressHUD(style: .light)
    //sourcery:end
    init(viewModel: VMSearch = VMSearch()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        tableViewDataSource = DSTableViewDataSource(viewModel: viewModel, viewController: self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        createConstraints()
        observeDataViewModel()
    }
    private func observeDataViewModel() {
        viewModel.repositoryData.subscribe(onNext: { [weak self] _ in
            self?.reloadData()
            self?.setupRepositoryLabelText()
            self?.setupEmptyView()
        }).disposed(by: disposeBag)
        searchBar.textField.rx.text
            .debounce(.milliseconds(500),
                      scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] text in
            self?.viewModel.searchString.accept(text)
        }).disposed(by: searchBar.disposeBag)
        viewModel.shouldShowLoader.subscribe(onNext: { [weak self] shouldShow in
            guard let sSelf = self else {return }
            if shouldShow {
                sSelf.hud.show(in: sSelf.view)
            } else {
                sSelf.hud.dismiss(animated: true)
            }
        }).disposed(by: disposeBag)
    }
    func setupCell(_ tableView: UITableView, cell: UITableViewCell, at indexPath: IndexPath) {
        guard let repoCell = cell as? GHARepoSearchCell,
            let object = viewModel.repositoryData.value?.items[indexPath.row] else {return }
        repoCell.setupCell(object: object)
    }
    private func reloadData() {
        UIView.transition(with: tableView,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: { self.tableView.reloadData() },
                          completion: nil)
    }
}
extension GHASearchTableViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            if !viewModel.downloadDataQueExist {
                viewModel.getDataForSearch()
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let object = viewModel.repositoryData.value?.items[indexPath.row] else {return }
        let viewC = GHAReposiotryDetailViewController(viewModel: VMRepositoryDetail(repositoryItem: object))
        navigationController?.pushViewController(viewC, animated: true)
    }
}
extension GHASearchTableViewController: ViewControllerAutoCreateViews {
    func setupViews() {
        setupTableView()
        tableView.keyboardDismissMode = .onDrag
        headerContainerView.backgroundColor = .ghaBlack
        totalResultsLabel.textColor = .white
        totalResultsLabel.numberOfLines = 1
        hud.textLabel.text = "GHASearchTableViewController_hudTitle".localized
        filterButton.setImage(UIImage(named: "icon_filters")?.withRenderingMode(.alwaysOriginal), for: .normal)
        filterButton.rx.tap.subscribe(onNext: { [weak self] _ in
            guard let sSelf = self else {return }
            let viewModel = VMOptionsSearch(searchOptions: sSelf.viewModel.searchOptions)
            let viewC = GHAOptionsSearchViewController(viewModel: viewModel)
            sSelf.navigationController?.pushViewController(viewC, animated: true)
        }).disposed(by: disposeBag)
        setupEmptyView()
    }
    func setupRepositoryLabelText() {
        let textForLabel = String(format: "GHASearchTableViewController_Results".localized,
                                  viewModel.repositoryData.value?.totalCount ?? 0)
        UIView.transition(with: totalResultsLabel,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: { self.totalResultsLabel.text = textForLabel },
                          completion: nil)
    }
    private func setupEmptyView() {
        var shouldHiddeView = false
        let repositoryData = viewModel.repositoryData.value
        let searchString = viewModel.searchString.value
        if let searchString = searchString,
            !searchString.isEmpty,
            repositoryData?.items.isEmpty ?? true,
            !viewModel.downloadDataQueExist {
            shouldHiddeView = false
            emptyView.setupView("GHASearchTableViewController_emptySearch".localized)
        } else if searchString == nil || searchString?.isEmpty ?? true {
            shouldHiddeView = false
            emptyView.setupView("GHASearchTableViewController_EnterText".localized)
        } else {
            shouldHiddeView = true
        }
        if emptyView.isHidden != shouldHiddeView {
            UIView.transition(with: emptyView,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: { self.emptyView.isHidden = shouldHiddeView },
                              completion: nil)
        }
    }
    func createConstraints() {
        var viewsDict = self.viewsDictionary
        viewsDict["tableView"] = tableView
        var constraints: [NSLayoutConstraint] = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[headerContainerView]|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[headerContainerView][tableView]|",
                                           options: [.alignAllLeading, .alignAllTrailing],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[searchBar]-16-[filterButton(30)]-16-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[totalResultsLabel]-16-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[searchBar(40)]-16-[totalResultsLabel]-16-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[emptyView]-20-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|->=0-[emptyView]->=0@750-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict)
            ].flatMap {$0 }
        constraints.append(contentsOf: [
            emptyView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
            searchBar.topAnchor.constraint(equalTo: safeTopAnchor, constant: 16),
            filterButton.centerYAnchor.constraint(equalTo: searchBar.centerYAnchor),
            filterButton.heightAnchor.constraint(equalTo: filterButton.widthAnchor, multiplier: 1)
        ])
        NSLayoutConstraint.activate(constraints)
    }
}
