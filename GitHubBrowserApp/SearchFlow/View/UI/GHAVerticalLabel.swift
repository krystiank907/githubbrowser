//
//  GHAVerticalLabel.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

class GHAVerticalLabel: UILabel {
    override func draw(_ rect: CGRect) {
        guard let text = self.text else {
            return
        }
        // Drawing code
        if let context = UIGraphicsGetCurrentContext() {
            let transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
            context.concatenate(transform)
            context.translateBy(x: -rect.size.height, y: 0)
            var newRect = rect.applying(transform)
            newRect.origin = CGPoint.zero
            let mutableCopy = NSMutableParagraphStyle.default.mutableCopy()
            guard let textStyle = mutableCopy as? NSMutableParagraphStyle else {return }
            textStyle.lineBreakMode = self.lineBreakMode
            textStyle.alignment = self.textAlignment
            let attributeDict: [NSAttributedString.Key: AnyObject] = [.font: font,
                                                                      .foregroundColor: textColor,
                                                                      .paragraphStyle: textStyle]
            let nsStr = text as NSString
            nsStr.draw(in: newRect, withAttributes: attributeDict)
        }
    }
}
