//
//  GHATextField.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 22/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

class GHATextField: UITextField {
    let padding = UIEdgeInsets(top: 3, left: 10, bottom: 3, right: 10)
    var tintedClearImage = UIImage(named: "icon_searchcanceldm")
    override func layoutSubviews() {
        super.layoutSubviews()
        self.tintClearImage()
    }
    private func tintClearImage() {
        for view in subviews {
            if let button = view as? UIButton {
                if let image = button.image(for: .highlighted) {
                    if self.tintedClearImage == nil {
                        tintedClearImage = self.tintImage(image: image, color: self.tintColor)
                    }
                    button.setImage(self.tintedClearImage, for: .normal)
                    button.setImage(self.tintedClearImage, for: .highlighted)
                }
            }
        }
    }
    private func tintImage(image: UIImage, color: UIColor) -> UIImage {
        let size = image.size
        UIGraphicsBeginImageContextWithOptions(size, false, image.scale)
        let context = UIGraphicsGetCurrentContext()
        image.draw(at: .zero, blendMode: CGBlendMode.normal, alpha: 1.0)
        context?.setFillColor(color.cgColor)
        context?.setBlendMode(CGBlendMode.sourceIn)
        context?.setAlpha(1.0)
        let rect = CGRect(x: CGPoint.zero.x, y: CGPoint.zero.y, width: image.size.width, height: image.size.height)
        UIGraphicsGetCurrentContext()?.fill(rect)
        let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return tintedImage ?? UIImage()
    }
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return  bounds.inset(by: padding)
    }
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
