//
//  GHASearchOptionCell.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

struct VMSearchOptionCell {
    var titleText: String
    var isOn: Bool
}

class GHASearchOptionCell: UITableViewCell, CellAutoCreateViews {
    weak var titleLabel: UILabel!
    weak var switchControl: UISwitch!
    weak var separatorView: UIView!
    //sourcery:begin: notCreate
    var disposeBag = DisposeBag()
    //sourcery:end
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createViews()
        setupViews()
        createConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupCell(_ viewModel: VMSearchOptionCell) {
        titleLabel.text = viewModel.titleText
        switch viewModel.isOn {
        case false:
            titleLabel.font = UIFont.appFont(ofSize: 16, weight: .regular)
            titleLabel.textColor = .white
        case true:
            titleLabel.font = UIFont.appFont(ofSize: 16, weight: .bold)
            titleLabel.textColor = .ghaOrange
        }
        switchControl.setOn(viewModel.isOn, animated: true)
    }
}
extension GHASearchOptionCell {
    func setupViews() {
        separatorView.backgroundColor = .white
    }
    func createConstraints() {
        let viewsDict = self.viewsDictionary

        var constraints: [NSLayoutConstraint] = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[titleLabel]-[switchControl]-16-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-16-[titleLabel]-16-[separatorView(1)]|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[separatorView]|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict)
            ].flatMap {$0 }
        constraints.append(contentsOf: [
            switchControl.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor)
            ])
        NSLayoutConstraint.activate(constraints)
    }
}
