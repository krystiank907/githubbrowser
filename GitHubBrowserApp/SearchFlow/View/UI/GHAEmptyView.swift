//
//  GHAEmptyView.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

class GHAEmptyView: UIView, AutoCreateViews {
    weak var iconGitImageView: UIImageView!
    weak var descriptionLabel: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        createViews()
        setupViews()
        createConstraints()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupViews() {
        iconGitImageView.image = UIImage(named: "iconGitHub")?.withRenderingMode(.alwaysTemplate)
        iconGitImageView.tintColor = .white
        iconGitImageView.contentMode = .scaleAspectFit
        descriptionLabel.font = UIFont.appFont(ofSize: 18, weight: .semibold)
        descriptionLabel.textAlignment = .center
        descriptionLabel.textColor = .white
    }
    func setupView(_ descriptionString: String) {
        descriptionLabel.text = descriptionString
    }
    func createConstraints() {
        let viewsDict = self.viewsDictionary
        var constraints: [NSLayoutConstraint] = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[iconGitImageView]-20-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[descriptionLabel]",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat:
                "V:|-20-[iconGitImageView]-20-[descriptionLabel]-20-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict)
            ].flatMap {$0 }
        constraints.append(contentsOf: [
            descriptionLabel.leadingAnchor.constraint(equalTo: iconGitImageView.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: iconGitImageView.trailingAnchor),
            iconGitImageView.heightAnchor.constraint(equalTo: iconGitImageView.widthAnchor, multiplier: 1)
        ])
        NSLayoutConstraint.activate(constraints)
    }
}
