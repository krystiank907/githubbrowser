//
//  GHARepoSearchCell.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

class GHARepoSearchCell: UITableViewCell, CellAutoCreateViews {
    weak var containerView: UIView!
    //sourcery:begin: superView = containerView
    weak var titleLabel: UILabel!
    weak var descriptionLabel: UILabel!
    weak var rightSeparatorView: UIView!
    weak var updateDateLabel: UILabel!
    //sourcery:end
    //sourcery:begin: superView = rightSeparatorView
    weak var languageVerticalLabel: GHAVerticalLabel!
    //sourcery:end
    //sourcery:begin: notCreate
    var heightConstraintLangugageLabel: NSLayoutConstraint!
    //sourcery:end
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createViews()
        setupViews()
        createConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupCell(object: GHARepository) {
        titleLabel.text = object.fullName
        descriptionLabel.text = object.description
        rightSeparatorView.backgroundColor = object.languageType?.color ?? UIColor(red: 0.01,
                                                                                   green: 0.05,
                                                                                   blue: 0.16,
                                                                                   alpha: 1.0)
        languageVerticalLabel.text = object.language
        heightConstraintLangugageLabel.constant = object.language.widthOfString() + 5
        if let updateDate = object.updatedAt {
            let dateString = DateFormatter.fullFormatDate.string(from: updateDate)
            updateDateLabel.text = String(format: "GHARepoSearchCell_updatedOn".localized, dateString)
        }
    }
}
extension GHARepoSearchCell {
    func setupViews() {
        containerView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 11
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 1
        titleLabel.font = UIFont.appFont(ofSize: 17, weight: .semibold)
        titleLabel.lineBreakMode = .byTruncatingTail
        descriptionLabel.textColor = .darkGray
        descriptionLabel.numberOfLines = 8
        descriptionLabel.lineBreakMode = .byWordWrapping
        containerView.addShadow(4)
        updateDateLabel.numberOfLines = 1
        updateDateLabel.textColor = .darkGray
        updateDateLabel.font = UIFont.appFont(ofSize: 14, weight: .light)
        languageVerticalLabel.textColor = .white
    }
    func createConstraints() {
        let viewsDict = self.viewsDictionary
        var constraints: [NSLayoutConstraint] = [
            NSLayoutConstraint.constraints(withVisualFormat:
                "V:|-[titleLabel]-[descriptionLabel]->=8-[updateDateLabel]-|",
                                           options: [.alignAllLeading, .alignAllTrailing],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat:
                "V:|[rightSeparatorView]|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat:
                "V:|->=5-[languageVerticalLabel]-5-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat:
                "H:|-3-[languageVerticalLabel]-3-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat:
                "H:|-19-[titleLabel]-16-[rightSeparatorView(26)]|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            containerView.constraintsEqualToSupperView(UIEdgeInsets(top: 15, left: 15, bottom: 0, right: 15))
            ].flatMap { $0 }
        heightConstraintLangugageLabel = languageVerticalLabel.heightAnchor.constraint(equalToConstant: 20)
        constraints.append(contentsOf: [ heightConstraintLangugageLabel ])
        NSLayoutConstraint.activate(constraints)
    }
}
