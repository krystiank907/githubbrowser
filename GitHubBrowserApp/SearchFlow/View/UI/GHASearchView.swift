//
//  GHASearchView.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 22/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RxSwift

class GHASearchView: UIView, AutoCreateViews {
    weak var bacgroundView: UIView!
    //sourcery:begin: superView = bacgroundView
    weak var textField: GHATextField!
    weak var iconImageView: UIImageView!
    //sourcery:end
    //sourcery:begin: notCreate
    var widthAnchorIcon: NSLayoutConstraint!
    var disposeBag = DisposeBag()
    //sourcery:end
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        createViews()
        setupViews()
        createConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func setupViews() {
        backgroundColor = .clear
        textField.attributedPlaceholder = NSAttributedString(string: "Search",
                                                             attributes: [.foregroundColor: UIColor.lightText])
        textField.textColor = UIColor.white
        textField.tintColor = UIColor.white
        textField.enablesReturnKeyAutomatically = true
        bacgroundView.clipsToBounds = false
        bacgroundView.layer.cornerRadius = 11
        bacgroundView.layer.borderWidth = 1
        bacgroundView.layer.borderColor = UIColor.darkGray.cgColor
        iconImageView.image = UIImage(named: "icon_searchsmalldm")
        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no
    }
    func createConstraints() {
        let viewsDict = self.viewsDictionary
        var constraints: [NSLayoutConstraint] = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[textField]-[iconImageView(16)]-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[textField]|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|->=0-[iconImageView(16)]->=0@750-|",
                                           options: [],
                                           metrics: nil,
                                           views: viewsDict),
            bacgroundView.constraintsEqualToSupperView()
            ].flatMap {$0 }
        widthAnchorIcon = iconImageView.widthAnchor.constraint(equalToConstant: 0)
        constraints.append(iconImageView.centerYAnchor.constraint(equalTo: textField.centerYAnchor))
        NSLayoutConstraint.activate(constraints)
    }
}
