//
//  GHAOptionsSearchViewController.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import DevStart

class GHAOptionsSearchViewController: GHABaseViewController, DSTableViewControllerWithViewModel {
    var viewModel: VMOptionsSearch!
    init(viewModel: VMOptionsSearch) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        tableViewDataSource = DSTableViewDataSource(viewModel: viewModel, viewController: self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    func setupCell(_ tableView: UITableView, cell: UITableViewCell, at indexPath: IndexPath) {
        guard let switchCell = cell as? GHASearchOptionCell else {return }
        switchCell.switchControl.isUserInteractionEnabled = false
        viewModel.searchOptions
            .subscribe(onNext: {[weak self] _ in
            guard let cellVm = self?.viewModel.vmForSwitchCell(at: indexPath) else {
                return
            }
            switchCell.setupCell(cellVm)
        }).disposed(by: disposeBag)
        let shouldHidde = indexPath.row + 1 == viewModel.numberOfItemsInTableSection(indexPath.section)
        switchCell.separatorView.isHidden = shouldHidde
    }
}
extension GHAOptionsSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.setState(for: indexPath)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = viewModel.headerTitleForSection(section) else { return nil }
        let headerView = UIView()
        headerView.backgroundColor = .ghaBlack
        headerView.addShadow(5)
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.appFont(ofSize: 16, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = header
        headerView.addSubview(label)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 10),
            label.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10),
            label.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 12),
            label.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -12)
            ])
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
}
