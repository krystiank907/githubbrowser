//
//  GHSearchRepositoriesType.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

// Not available list of all languages form gitHab, so only for static pick
enum GHSearchRepositoriesType: Int, CaseIterable {
    case all = 0
    case javaScript
    case java
    case cpp
    case python
    case cSharp
    case assembly
    case cLanguage
    case html
    case typeScript
    case swift
    var paramName: String {
        switch self {
        case .javaScript:
            return "JavaScript"
        case .java:
            return "Java"
        case .cpp:
            return "C++"
        case .python:
            return "Python"
        case .cSharp:
            return "C#"
        case .assembly:
            return "Assembly"
        case .cLanguage:
            return "C"
        case .html:
            return "HTML"
        case .typeScript:
            return "TypeScript"
        case .swift:
            return "Swift"
        case .all:
            return "All"
        }
    }
    var color: UIColor {
        switch self {
        case .javaScript:
            return UIColor(red: 0.06, green: 0.50, blue: 0.50, alpha: 1.0)
        case .java:
            return UIColor(red: 0.50, green: 0.50, blue: 0.02, alpha: 1.0)
        case .cpp:
            return UIColor(red: 0.40, green: 0.40, blue: 1.00, alpha: 1.0)
        case .python:
            return UIColor(red: 0.99, green: 0.50, blue: 0.03, alpha: 1.0)
        case .cSharp:
            return UIColor(red: 0.07, green: 0.50, blue: 0.01, alpha: 1.0)
        case .assembly:
            return UIColor(red: 0.50, green: 0.00, blue: 0.01, alpha: 1.0)
        case .cLanguage:
            return UIColor(red: 0.99, green: 0.44, blue: 0.81, alpha: 1.0)
        case .html:
            return UIColor(red: 0.54, green: 0.73, blue: 0.92, alpha: 1.0)
        case .typeScript:
            return UIColor(red: 0.71, green: 0.93, blue: 0.34, alpha: 1.0)
        case .swift:
            return UIColor(red: 0.95, green: 0.61, blue: 0.00, alpha: 1.0)
        default:
            return UIColor()
        }
    }
}
