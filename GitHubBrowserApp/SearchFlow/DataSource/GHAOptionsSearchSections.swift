//
//  GHAOptionsSearchSections.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

enum GHAOptionsSearchSections: Int, CaseIterable {
    case filters = 0, sort
    var title: String {
        return "GHAOptionsSearchSections_\(self)".localized
    }
}
