//
//  GHSearchRepositoriesSortType.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

enum GHSearchRepositoriesSortType: Int, CaseIterable {
    case bestMatch = 0
    case stars
    case forks
    case updated
    var paramName: String {
        return "\(self)"
    }
    var name: String {
        return "GHSearchRepozitoriesSortType_\(self)".localized
    }
}
