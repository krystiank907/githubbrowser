//
//  GHSearchOrderType.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

enum GHSearchOrderType: Int, CaseIterable {
    case desc = 0
    case asc
    var paramName: String {
        return "\(self)"
    }
}
