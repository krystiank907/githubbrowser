//
//  GHSearchParams.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

struct GHSearchParams: GHParams {
    var searchString: String
    var languageTypeInt: Int = 0
    var sortTypeInt: Int?
    var orderTypeInt: Int?
    var perPage: Int = 100
    var page: Int = 0
    var languageType: GHSearchRepositoriesType {
        get {
            return GHSearchRepositoriesType(rawValue: languageTypeInt) ?? .all
        } set {
            languageTypeInt = newValue.rawValue
        }
    }
    var sortType: GHSearchRepositoriesSortType? {
        get {
            guard let sortTypeInt = sortTypeInt else { return nil }
            return GHSearchRepositoriesSortType(rawValue: sortTypeInt)
        } set {
            sortTypeInt = newValue?.rawValue
        }
    }
    var orderType: GHSearchOrderType? {
        get {
            guard let orderTypeInt = orderTypeInt else { return nil }
            return GHSearchOrderType(rawValue: orderTypeInt)
        } set {
            orderTypeInt = newValue?.rawValue
        }
    }
    init(searchString: String) {
        self.searchString = searchString
    }
    func dictRepresentation() -> [String: Any] {
        var dict = [String: Any]()
        if languageType != .all {
            dict["q"] = "\(searchString)+language:\(languageType.paramName)"
        } else {
            dict["q"] = searchString
        }
        if let sortType = sortType, sortType != .bestMatch {
            dict["sort"] = sortType.paramName
        }
        if let orderType = orderType {
            dict["order"] = orderType.paramName
        }
        dict["page"] = page
        dict["per_page"] = perPage
        return dict
    }
}
