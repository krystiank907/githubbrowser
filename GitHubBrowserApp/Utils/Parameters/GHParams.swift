//
//  GHParams.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

public protocol GHParams {
    func dictRepresentation() -> [String: Any]
}
