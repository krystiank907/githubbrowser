//
//  GHAResponse.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

enum GHAResponse {
    case succes(data: Any?)
    case failure(error: Error?)
    var error: Error? {
        switch self {
        case .succes:
            return nil
        case .failure(let error):
            return error
        }
    }
    var data: Any? {
        switch self {
        case .succes(let data):
            return data
        case .failure:
            return nil
        }
    }
}
