//
//  GHASaveManager.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

class GHASaveManager {
    static let shered = GHASaveManager()
    func processData(dataType: GHAMethodInfo,
                     response: GHAResponse,
                     completionHandler: ConnectionCompletionHandler?) {
        switch response {
        case .succes(let data):
            if let dataValue = data as? Data,
                let json = try? JSONSerialization.jsonObject(with: dataValue, options: []) as? [String: Any] {
                let object = dataType.objects(from: json)
                completionHandler?(.succes(data: object))
            } else {
                completionHandler?(.failure(error: nil))
            }
        default:
            completionHandler?(response)
        }
    }
}
