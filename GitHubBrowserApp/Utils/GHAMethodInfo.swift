//
//  GHAMethodInfo.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import Alamofire

enum GHAMethodInfo {
    case searchRepository(params: GHSearchParams)
    var httpMethod: HTTPMethod {
        switch self {
        case .searchRepository:
            return .get
        }
    }
    var methodName: String {
        switch self {
        case .searchRepository:
            return "search/repositories"
        }
    }
    var params: GHParams {
        switch self {
        case .searchRepository(let params):
            return params
        }
    }
    var server: GHAServerType {
        switch self {
        case .searchRepository:
            return .gitHub
        }
    }
    func objects(from json: Any) -> Any? {
        switch self {
        case .searchRepository:
            return (try? JSONDecoder().decode(GHARepositoriesData.self, from: json))
        }
    }
    var urlString: String {
        let url = server.serverUrl + "/" + methodName
        return url
    }
}
