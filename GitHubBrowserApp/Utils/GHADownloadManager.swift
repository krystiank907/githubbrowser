//
//  GHADownloadManager.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

typealias ConnectionCompletionHandler = (_ response: GHAResponse) -> Void

class GHADownloadManager {
    static let shered = GHADownloadManager()
    var saveManager = GHASaveManager.shered
    var sessionManager: Session = Session(configuration: URLSessionConfiguration.default)
    func downloadData(methodInfo: GHAMethodInfo, completionHandler: ConnectionCompletionHandler?) {
        let encoding: ParameterEncoding = methodInfo.httpMethod == .get ? NOURLEncoding() : JSONEncoding.default
        let parameters = methodInfo.params.dictRepresentation()
        let urlString = methodInfo.urlString
        let httpMethod = methodInfo.httpMethod
        let httpRequest = sessionManager.request(urlString,
                                                 method: httpMethod,
                                                 parameters: parameters,
                                                 encoding: encoding,
                                                 interceptor: nil)
        httpRequest.responseJSON { [weak self] response in
            guard let self = self else {
                completionHandler?(.failure(error: nil))
                return
            }
            switch response.result {
            case .success:
                self.saveManager.processData(dataType: methodInfo,
                                             response: .succes(data: response.data),
                                             completionHandler: completionHandler)
            case .failure(let error):
                completionHandler?(.failure(error: error))
            }
        }
    }
}
