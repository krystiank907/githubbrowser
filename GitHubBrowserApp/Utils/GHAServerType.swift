//
//  GHAServerType.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

enum GHAServerType {
    case gitHub
    var serverUrl: String {
        switch self {
        case .gitHub:
            return "https://api.github.com"
        }
    }
}
