//
//  DateFormatter+Ext.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 22/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

extension DateFormatter {
    static var defaultFormat: DateFormatter {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return format
    }
    static var shortFormat: DateFormatter {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd"
        return format
    }
    static var fullFormatDate: DateFormatter {
        let format = DateFormatter()
        format.dateFormat = "dd MMM yyyy"
        return format
    }
}
