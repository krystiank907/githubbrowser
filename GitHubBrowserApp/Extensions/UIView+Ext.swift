//
//  UIView+Ext.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 22/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

extension UIView {
    public func addShadow(_ radius: CGFloat = 10.0,
                          offset: CGSize = CGSize.zero,
                          color: UIColor = UIColor.darkGray) {
        let layer = self.layer
        let maskToBound = layer.masksToBounds
        layer.masksToBounds = false
        layer.addShadow(radius, offset: offset, color: color)
        layer.masksToBounds = maskToBound
        let backgroundCGColor = self.backgroundColor?.cgColor
        self.backgroundColor = .clear
        layer.backgroundColor =  backgroundCGColor
    }
}
