//
//  String+Ext.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

extension String {

    func widthOfString(usingFont font: UIFont = UIFont.appFont()) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont = UIFont.appFont()) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func sizeOfString(usingFont font: UIFont = UIFont.appFont()) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
}
