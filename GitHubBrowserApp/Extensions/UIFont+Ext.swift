//
//  UIFont+Ext.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

extension UIFont {
    static func appFont(ofSize size: CGFloat = 17, weight: UIFont.Weight = .regular) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: weight)
    }
}
