//
//  UIColor+Ext.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 23/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

extension UIColor {
    static var ghaOrange: UIColor {
        return UIColor(red: 0.90, green: 0.49, blue: 0.13, alpha: 1.0)
    }
    static var ghaBlack: UIColor {
        return UIColor(red: 0.15, green: 0.15, blue: 0.15, alpha: 1.0)
    }
}
