//
//  CALayer+Ext.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 22/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

extension CALayer {
    func addShadow(_ radius: CGFloat = 10.0,
                   offset: CGSize = CGSize.zero,
                   color: UIColor = UIColor.darkGray) {
        shadowOpacity = 0.5
        shadowOffset = offset
        shadowRadius = radius
        shadowColor = color.cgColor
    }
}
