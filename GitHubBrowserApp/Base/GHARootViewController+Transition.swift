//
//  GHARootViewController+Transition.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

extension GHARootViewController: UIViewControllerAnimatedTransitioning {
    var transitionDuration: TimeInterval {
        return 0.15
    }
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return transitionDuration
    }
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from),
            let toViewController = transitionContext.viewController(forKey: .to) else { return }
        guard let fromIndex = viewControllers?.firstIndex(of: fromViewController),
            let toIndex = viewControllers?.firstIndex(of: toViewController), fromIndex != toIndex else { return }
        let toView: UIView = toViewController.view
        let containterView: UIView = transitionContext.containerView
        let containerBounds = containterView.bounds
        containterView.addSubview(toView)
        toView.frame = containerBounds
        toView.alpha = 0
        let translationX = fromIndex < toIndex ? containerBounds.width : -containerBounds.width
        toView.transform = CGAffineTransform(translationX: translationX * 0.5, y: 0)
        UIView.animate(
            withDuration: transitionDuration,
            delay: 0,
            options: [.curveEaseOut],
            animations: {
                toView.alpha = 1
                toView.transform = .identity
        }, completion: { _ in
            transitionContext.completeTransition(true)
        })
    }
}
