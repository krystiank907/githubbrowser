//
//  GHABaseViewController.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import DevStart

class GHABaseViewController: DSViewController {
    var prefersNavigationBarHidden: Bool { return false }
    var downloadManager: GHADownloadManager {
        return GHADownloadManager.shered
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(prefersNavigationBarHidden, animated: false)
    }
    override func setupView() {
        super.setupView()
        view.backgroundColor = UIColor(red: 0.09, green: 0.09, blue: 0.09, alpha: 1.0)
    }
    func setTitleNavBar(_ title: String) {
        navigationItem.title = title
    }
}
