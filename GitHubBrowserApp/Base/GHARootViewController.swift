//
//  GHARootViewController.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

class GHARootViewController: UITabBarController {
    init() {
        super.init(nibName: nil, bundle: nil)
        createViewControllers()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }
    private func createViewControllers() {
        var viewControllers = [UIViewController]()
        for item in GHARootTabBarItem.allCases {
            var viewController: UIViewController
            switch item {
            case .search:
                viewController = UINavigationController(rootViewController: GHASearchTableViewController())
            case .favourites:
                viewController =
                    UINavigationController(rootViewController: GHARepoFavouriteViewController())
            }
            viewController.tabBarItem = item.tabbarItem
            viewControllers.append(viewController)
        }
        setViewControllers(viewControllers, animated: false)
    }
}
extension GHARootViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController,
                          animationControllerForTransitionFrom fromVC: UIViewController,
                          to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}
