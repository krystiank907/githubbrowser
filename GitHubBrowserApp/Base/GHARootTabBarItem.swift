//
//  GHARootTabBarItem.swift
//  GitHubBrowserApp
//
//  Created by Krystian Kulawiak on 21/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import DevStart
import UIKit

enum GHARootTabBarItem: Int, CaseIterable {
    case search
    case favourites
    var iconNormal: UIImage? {
        return UIImage(named: "TabBar_Icon_\(self)")?.withRenderingMode(.alwaysOriginal)
    }
    var iconSelected: UIImage? {
        return UIImage(named: "TabBar_Icon_\(self)_selected")?.withRenderingMode(.alwaysOriginal)
    }
    var title: String {
        return "Tabber_Title_\(self)".localized
    }
    var tabbarItem: UITabBarItem {
        let tabBarItem = UITabBarItem(title: title, image: iconNormal, selectedImage: iconSelected)
        tabBarItem.imageInsets = UIEdgeInsets(top: -6, left: 0, bottom: 0, right: 0)
        return tabBarItem
    }
}
